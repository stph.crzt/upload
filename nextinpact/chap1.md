Alice et Bob marchent côte à côté. Ils ne se tiennent pas par la main, pourtant, Bob, il voudrait bien.

— Tu vas en cours ce matin ? demande Alice avec un petit sourire qui semble ajouter, même si je sais bien que t'y vas pas souvent.

— Je sais pas, c'est quoi au menu ?

— Initiation à la programmation, on va faire du JavaScript.

— On a pas déjà fait ça au trimestre dernier ?

— C'est un on qui t'inclut pas, hein ? Je t'y ai pas vu souvent en cours le trimestre dernier ! répond Alice avec malice. Non, on a surtout vu comment se servir de l'ordi, gérer son système, et on a vu comme écrire et publier du contenu sur le Web via les Rasp de l'asso. On a fait un tout petit peu de JS, mais...

— de JS ?

— JavaScript. T'es largué ! Bref, là on va vraiment apprendre à écrire des programmes, c'est la partie que je préfère. À la fin on va programmer un petit robot. C'est César du repair café qu'il l'a fabriqué.

— Je pense que je vais aller au jardin plutôt.

— Y a rien à y faire en hiver !

— Si, si, y a toujours des trucs à faire. Y a la serre à ranger. Et je vais finir de pailler, on a récolté les dernières courges et il reste des coins où la terre est à nu. Le paillage, ça prépare un peu d'humus pour le printemps, et puis ça limite les adventices.

— Les adventices ? T'en connais des mots quand tu veux !

— Les herbes indésirables.

— Ha, les mauvaises herbes...

— C'est toi la mauvaise herbe ! la coupe-t-il en riant. On dit plus comme ça. L'herbe elle est mauvaise que par rapport à ce qu'on veut faire nous les humains à un moment donné avec, mais pas en général. Elle rend des services aussi. C'est important les mots, ça force à réfléchir un peu plus. Ça construit notre représentation de la réalité, même.

— Merci pour la leçon.

Alice est un peu piquée, mais finalement elle se dit que Bob est pas si ballot qu'il veut bien le laisser paraître, il doit être plus assidu aux cours de philo. Et sur son jardin, il est à fond, elle trouve ça mignon.

— Tu vois les orties, c'est la plaie, hein, ça pique sa race, ça se faufile partout, tu galères à t'en débarrasser. Et ben tu peux faire des tas de trucs avec, de l'engrais, du désherbant, même ça se bouffe. J'ai essayé, je me suis cramé la gorge et j'ai eu l'impression d'avoir une otite pendant trois jours, mais parait que je m'y suis mal pris, qu'il faut manger que les jeunes pousses. Je réessaierai.

Et puis, Alice, elle se dit que quand elle reprogrammera le monde, il faudra bien des types comme lui pour lui préparer à manger. Elle rêvasse à elle vautrée derrière son PC et Bob à la cuisine en train de lui préparer une omelette fromage et orties qui pique un peu à l'intérieur des oreilles.

— On se retrouve cet aprem alors, tu vas au wushu ?

Ils arrivent à l'école. C'est des anciens bureaux de la ville qui devaient être détruits, trop vieux, trop à l'écart. Finalement, ils les ont filé à l'asso. Petit à petit chaque nouvelle promo rénove, de l'isolation surtout. Avec du chanvre qu'on fait pousser nous même. Y a toujours plein de volontaires pour faire pousser du chanvre. C'est pas du style second empire, plutôt un gros cube décrépit planté au milieu de nulle part, et en hiver, avec les champs rasés de frais et la forêt sans feuilles, c'est un peu triste. Mais le méga-tag plein de couleurs qui tartine toute la façade donne la patate quand tu t'approches. Il annonce dans un style à la fois foutraque et fier : UPLOAD. Université Populaire Libre Ouverte Accessible Décentralisée. Et en plus petit en dessous : Technologie, Écologie, Humanité.

Pourtant, Bob fait un peu la gueule, comme à chaque fois qu'ils arrivent et qu'il n'a pas réussi à lui parler d'autre chose que de son jardin à Alice. Je t'aime Alice ! Voilà. Ben non. À tout à l'heure Alice.
